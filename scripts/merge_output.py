import pandas


def main(counts_path, haystack_results, metacycle_results, time_points, outpath):
    """ merges haystack and metacycle results into single table for further
        processing. I plan to add some visualizations at some point
    """
    meta = pandas.read_csv(metacycle_results, sep="\t", index_col=0)
    hay = pandas.read_csv(haystack_results, sep="\t", index_col=0)
    merged = pandas.concat([hay, meta], axis=1, keys=["haystack", "metacycle"])
    merged.columns = ["_".join(p) for p in merged.columns]
    merged.to_csv(outpath, sep="\t")


def snakemain():
    meta, haystack, counts = snakemake.input
    outpath = snakemake.output[0]
    times = snakemake.params["time_points"]
    main(counts, haystack, meta, times, outpath)


if(__name__ == "__main__"):
    snakemain()
    # quick_test()