import numpy
import pandas
import json

""" Model types:
    sin
    linear interpolation
    square interpolation
"""


def sin_gen(phase, period):
    """ Generates sin functions with defined phase and period

        f(phase, period)(x) ~ sin((x - phase) / period * 2 * pi)
    """
    divisor = period / 2 / numpy.pi
    offset = phase

    def ret(tps):
        return numpy.cos((tps - offset) / divisor)
    return ret


def spike_gen(phase, period, slope_width, high_width, lv=-1, hv=1):
    """ Generates a spike/square like function using interpolation

        phase - Where function should peak
        period - how often function cycles
        slope_width - How much of interval to spend on a transition
        high_width - how much of interval to spend at high value
        lv - low value
        hv - high value
    """
    hp1 = (phase + high_width / 2) % period
    hp2 = (phase - high_width / 2) % period
    lp1 = (hp1 + slope_width) % period
    lp2 = (hp2 - slope_width) % period
    xp = [hp1, hp2, lp1, lp2]
    fp = [hv] * 2 + [lv] * 2

    def ret(tps):
        return numpy.interp(tps, xp, fp, period=period)
    return ret


def decorelate(df, threshold=1):
    """ Remove rows from df when they are found to correlate too well with a
        previous row
    """
    # creat mapping from models to the set of models that are too similar
    cor_map = (df.transpose().corr() >= threshold).apply(
        lambda s: list(set(s[s].index))
    )
    nondups = set()
    for m, fam in cor_map.items():
        if(len(fam & nondups) == 0):
            nondups.add(m)
    print(df.shape)
    df = df[df.index.isin(nondups)]
    print(df.shape)
    return df


def make_haystack_models(time_points, sin_params, spike_params, outfile):
    time_points = numpy.array(time_points)
    sin_models = [sin_gen(*args) for args in sin_params]
    sin_names = [
        f"mt=cos_lag={a[0]}_period={a[1]}"
        for a in sin_params
    ]
    spike_models = [spike_gen(*args) for args in spike_params]
    spike_names = [
        f"mt=ct_lag={a[0]}_period={a[1]}_sw={a[2]}_hw={a[3]}"
        for a in spike_params
    ]
    models = pandas.DataFrame(
        [m(time_points) for m in sin_models + spike_models],
        columns=time_points,
        index=sin_names + spike_names
    )
    # models = decorelate(models)
    models = models[models.std(axis=1) != 0]
    print(models.shape)
    print(models.index.str.split("_").str[1].str.split("=").str[1].value_counts())
    models.to_csv(outfile, sep="\t", header=None)
    return models.transpose().corr()


def snakemain():
    outfile = snakemake.output[0]
    time_points = snakemake.params["timepoints"]
    sin_params = snakemake.params["sinspace"]
    spike_params = snakemake.params["spikespace"]
    make_haystack_models(time_points, sin_params, spike_params, outfile)


def test():
    periods = [24]
    phase_step = 1
    spike_step = 4
    time_points = [0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48]

    sinspace = [(ph, per) for per in [24] for ph in range(0, per, phase_step)]
    spikespace = [
        (ph, per, spike_step, hw)
        for per in periods
        for ph in range(0, per, phase_step)
        for hw in range(int(spike_step / 2), per - spike_step * 2, spike_step)
        if(hw + 2 * spike_step <= per)
    ] + [
        (ph, per, per / 2, 0)
        for per in periods
        for ph in range(0, per, phase_step)
    ]
    print(pandas.DataFrame(spikespace)[0].value_counts())

    import os
    testout = os.path.join(os.path.expanduser("~"), "test_models.txt")
    corrdf = make_haystack_models(time_points, sinspace, spikespace, testout)
    print(corrdf.shape)

    for x in spike_gen(23, 24, 12, 0)(time_points):
        print(x)


if(__name__ == "__main__"):
    snakemain()
    # test()
