import pandas
import random
import numpy


def main(counts_path, resamples_out):
    """ dumps headless version of user counts followed by doing a bunch of
        row-wise shuffles of the data for bootstrap purposes.
    """
    counts = pandas.read_csv(counts_path, sep="\t", index_col=0, header=None)
    # counts.to_csv(counts_out, sep="\t", header=None)
    for f in resamples_out:
        samp = counts.apply(
            lambda s: pandas.Series(random.sample(list(s), len(s)), index=s.index),
            axis=1
        )
        # norm_func(samp).to_csv(f, sep="\t", header=None)
        samp.to_csv(f, sep="\t", header=None)


def snakemain():
    counts_path = snakemake.input[0]
    resamples_out = snakemake.output["resamps"]
    main(counts_path, resamples_out)


if(__name__ == "__main__"):
    snakemain()
    # quick_test()
