import matplotlib
matplotlib.use('Agg')

import numpy
import pandas
import os
from matplotlib import pyplot

import cycling_misc as cm


def make_pie(
    counts: pandas.DataFrame,
    haystack: pandas.DataFrame,
    outpath: str
) -> None:
    """ build a pie chart representing all genes
    """
    tot_genes = len(counts)
    expressed_genes = len(haystack)
    cycling_genes = sum(haystack.is_cycler)
    exp_non_cyc = expressed_genes - cycling_genes
    non_exp = tot_genes - expressed_genes
    pyplot.pie(
        [non_exp, exp_non_cyc, cycling_genes],
        labels=[
            f"non_expressed : {non_exp}",
            f"non_cycling : {exp_non_cyc}",
            f"cycling : {cycling_genes}"
        ],
        autopct="%1.1f%%"
    )
    pyplot.title("Breakdown of Genes")
    pyplot.tight_layout()
    pyplot.savefig(outpath)
    pyplot.close()


def pie_cyclers(
    cyclers: pandas.DataFrame,
    outpath: str
) -> None:
    """ Build a pie chart representing cyclers
    """
    cycler_counts = cyclers.lag.value_counts().sort_index()
    pyplot.pie(
        list(cycler_counts),
        labels=cycler_counts.index,
    )
    pyplot.title("Breakdown of Cyclers")
    pyplot.tight_layout()
    pyplot.savefig(outpath)
    pyplot.close()


def make_heat(
    cyclers: pandas.DataFrame,
    outpath: str,
) -> None:
    """ Build a heatmap represneting the cyclers
    """
    sort_cyclers = cyclers.set_index(["lag", "gene_id"]).sort_index().iloc[:, 4: -3].transpose()
    m = sort_cyclers.mean()
    s = sort_cyclers.std()
    sort_cyclers -= m
    sort_cyclers /= s
    df = sort_cyclers.transpose().reset_index().iloc[:, 2:]
    pyplot.pcolormesh(df)
    pyplot.yticks([])
    pyplot.xticks(numpy.arange(0.5, len(df.columns), 1), df.columns)
    pyplot.ylabel("Cycling Genes")
    pyplot.xlabel("Expression")
    pyplot.title("Standardized Expression of Cyclers")
    pyplot.tight_layout()
    pyplot.savefig(outpath)
    pyplot.close()


def make_waves(
    cyclers: pandas.DataFrame,
    outpath: str
) -> None:
    """ Create the "wave" plots
    """
    sort_cyclers = cyclers.set_index(["lag", "gene_id"]).sort_index().iloc[:, 4: -3].transpose()
    m = sort_cyclers.mean()
    s = sort_cyclers.std()
    sort_cyclers -= m
    sort_cyclers /= s
    sort_cyclers = sort_cyclers.transpose()
    sort_cyclers.to_csv("/home/nhartwic/test.tsv", sep="\t")
    rows = 6
    fig_height = int(rows * 3) + 0.5
    fig, axs = pyplot.subplots(rows, 4, figsize=(16, fig_height), squeeze=False)
    axs = [ax[i] for i in range(len(axs[0])) for ax in axs]
    colors = [(0.3, 1 - i / len(axs), i / len(axs)) for i in range(len(axs))]

    for l, ax, c in zip(range(len(axs)), axs, colors):
        if(l in sort_cyclers.index.get_level_values(0)):
            express = sort_cyclers.loc[l]
            express.transpose().plot(
                legend=False,
                title=f"All cyclers with phase={l}",
                ax=ax,
                color=[c] * express.shape[1],
                alpha=0.2
            )
    pyplot.suptitle(
        "Standardized Expression of Cycling Genes",
        y=(fig_height - 0.25) / fig_height
    )
    pyplot.tight_layout(rect=[0, 0, 1, (fig_height - 0.5) / fig_height])
    pyplot.savefig(outpath)
    pyplot.close()


def main(
    counts_path: str,
    haystack_path: str,
    haystack_pie_path: str,
    cyclers_pie_path: str,
    cyclers_heat_path: str,
    cyclers_waves_path: str,
) -> None:
    """ Main function which will build all desired figures
    """
    counts = cm.load_counts(counts_path)
    hay = cm.load_haystack_output(haystack_path)
    make_pie(counts, hay, haystack_pie_path)
    cyclers = hay[hay.is_cycler]
    pie_cyclers(cyclers, cyclers_pie_path)
    make_heat(cyclers, cyclers_heat_path)
    make_waves(cyclers, cyclers_waves_path)


def snake_main() -> None:
    """ pass snakemake data to main
    """
    main(
        snakemake.params["counts"],
        snakemake.input["hay_out"],
        snakemake.output["haystack_pie"],
        snakemake.output["cyclers_pie"],
        snakemake.output["cyclers_heat"],
        snakemake.output["cyclers_wave"]
    )


if(__name__ == "__main__"):
    snake_main()
