import pandas
import random
import numpy


def none_normalization(df):
    """ This is an identity function - lambda x: x
    """
    return df


def quantile_normalization(df):
    """ Apply quantile normalization to a dataframe and return normalized dataframe
    """
    rank_vals = {}
    ranks = df.rank(method="min").astype(int)
    for col in df.columns:
        for rank, val in zip(ranks[col], df[col]):
            temp = rank_vals.get(rank, [])
            temp.append(val)
            rank_vals[rank] = temp
    rank_vals = {r: sum(l) / len(l) for r, l in rank_vals.items()}
    return ranks.applymap(rank_vals.get)


def main(counts_path, counts_out, norm_type, threshold):
    """ applies a specified normalization to a counts table
    """
    norm_func = {
        "none": none_normalization,
        "quantile": quantile_normalization,
        "quantile_E": lambda df: quantile_normalization(df.applymap(lambda e: 10 ** e)),
        "quantile_log": lambda df: quantile_normalization(df.applymap(numpy.log10).clip(-5))
    }[norm_type]
    counts = pandas.read_csv(counts_path, sep="\t", index_col=0)
    normed = norm_func(counts)
    normed = normed[normed.mean(axis=1) > threshold]
    normed.to_csv(counts_out, sep="\t", header=None)


def snakemain():
    counts_path = snakemake.input[0]
    counts_out = snakemake.output[0]
    norm = snakemake.params["norm"]
    threshold = snakemake.params["threshold"]
    main(counts_path, counts_out, norm, threshold)


def quick_test():
    df = pandas.DataFrame({
        "a": [1, 2],
        "b": [2, 3],
        "c": [6, 90],
        "d": [92, 5]
    })
    print(quantile_normalization(df))

if(__name__ == "__main__"):
    snakemain()
    # quick_test()