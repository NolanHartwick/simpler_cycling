import pandas
from statsmodels.stats.multitest import multipletests
import numpy


def parse_model_str(s):
    """ parse model strings as given in our model file. Is used by
        load_haystack_output
        ### WARNING ### This feature requires a particularly well formatted
        model file and will likely break
    """
    temp = dict(f.split("=") for f in s.split("_"))
    modtype, per, lag = temp["mt"], temp["period"], temp["lag"]
    keys = ["lag", "period", "model_type"]
    return dict(zip(keys, [int(lag), int(per), modtype]))


def load_haystack_output(fpath, parse_model=True):
    """ Parse raw output from haystack into a dataframe
    """
    colnames = ["gene_id", "model_id", "R", "T", "P"]
    data = pandas.read_csv(fpath, sep="\t", header=None).iloc[:, 1: -1]
    timepoints = len(data.columns) - len(colnames)
    timepoints = [f"t{n}" for n in range(timepoints)]
    data.columns = colnames + timepoints
    if(parse_model):
        model_info = data["model_id"].apply(parse_model_str)
        data["lag"] = model_info.apply(lambda x: x["lag"])
        data["period"] = model_info.apply(lambda x: x["period"])
        data["model_type"] = model_info.apply(lambda x: x["model_type"])
    data['R'] = pandas.to_numeric(data.R, errors='coerce')
    return data, timepoints


def main(haystack_path, resamps, outpath, time_points, outpath_large):
    df, anon_times = load_haystack_output(haystack_path)
    resamps = [load_haystack_output(f)[0] for f in resamps]
    resamp_R = numpy.array(sorted([r for df in resamps for r in df.R.dropna()]))
    df["rank_r"] = df.R.apply(lambda v: numpy.searchsorted(resamp_R, v))
    df["resamp_p"] = 1 - df.rank_r / len(resamp_R)
    df["FDR"] = multipletests(df.resamp_p, method="fdr_bh")[1]
    passed_keys = ["gene_id", "model_id", "lag", "period", "model_type", "R", "T", "P", "FDR"]
    df[passed_keys].to_csv(outpath, sep="\t", index=False)
    large = df[passed_keys + anon_times]
    large.columns = passed_keys + time_points
    large.to_csv(outpath_large, sep="\t", index=False)


def snakemain():
    hayin = snakemake.input["users"]
    resamps = snakemake.input["resamps"]
    time_points = snakemake.params["time_points"]
    outpath = snakemake.output[0]
    outpath_large = snakemake.output[1]
    main(hayin, resamps, outpath, time_points, outpath_large)


if(__name__ == "__main__"):
    snakemain()
