# simpler_cycling
This is a simple workflow that runs haystack (with resampling for estimating FDR) and metacycle to identify rhythmic genes in an RNAseq experiment. 

The main input of this tool is a counts table. For experiments focussed on 24 hour rhythms, no other parameters are really needed, though the filter parameter may be modified depending on how deep your sequencing was.

Your counts table should have K+1 columns where K is your number of samples, the extra column is for gene IDs. Your first row should specify the time points for your samples. Your table could look something like...

|Name                 |4        |8        |12       |16       |20       |24       |28       |32      |36       |40       |44       |48       |
|---------------------|---------|---------|---------|---------|---------|---------|---------|--------|---------|---------|---------|---------|
|P39.liftoff.chr1.10  |0        |0        |0        |0        |0        |0        |0        |0       |0        |0.237544 |0        |0        |
|P39.liftoff.chr1.1000|0        |0        |0        |0        |0        |0        |0        |0       |0        |0        |0        |0        |
|P39.liftoff.chr1.101 |10.814819|12.929763|10.659097|24.153463|23.742912|12.480993|11.000875|6.763319|18.573944|9.264204 |31.906801|21.717755|
|P39.liftoff.chr1.1017|7.13502  |2.294204 |1.806627 |65.036756|30.700718|10.506204|6.774492 |2.105206|1.103403 |60.337527|13.504374|8.294715 |
|P39.liftoff.chr1.1018|0        |0        |0        |0.842563 |0        |0        |0        |0       |0        |0        |0        |0        |

...If your table has more complex sample labels, you can either make a modified table, or pass in the time point labels via the "time_points" optional config parameter.

Running should be as simple as...

```
ls your_count_table.tsv
git clone git@gitlab.com:NolanHartwick/simpler_cycling.git
# mamba is an alternative to conda meant to be a drop in replacement. use "conda" if you prefer it
mamba env create -n cycling -f simpler_cycling/envs/general.yml
mamba activate cycling
snakemake --snakefile simpler_cycling/Snakefile -j 8 --config counts=your_count_table.tsv
```

...Final results could then be found in "cycling_results.tsv". Columns are:

0. implicit_column (Gene ID): A gene from your counts table
1. haystack_model_id : An id referencing a row from "haystack_models.tsv"
2. haystack_lag : When expression is estimated to peak
3. haystack_period : How long between peaks in expression
4. haystack_model_type : the type associated with this haystack_model_id 
5. haystack_R : The correlation between 
6. haystack_T : The T statistic associated with the correlation (I think haystack mislabels this and it should be an F statistic)
7. haystack_P : The P value associated with this gene. Low values imply the rhythm observed was unlikely to be a result of change
8. haystack_FDR : An estimated FDR for the haystack_P value based on resambling time points
9. metacycle_JTK_pvalue: an estimated P value from JTK (a component in metacycle)
10. metacycle_JTK_BH.Q : an estimated FDR value from JTK 
10. metacycle_JTK_period : an estimated period from JTK 
11. metacycle_JTK_adjphase : An estiamted lag from JTK 
12. metacycle_JTK_amplitude : An estimated amplitude (peak - trough) from JTK 
13. metacycle_LS_pvalue : An estimated P value from LS (a component in metacycle)
14. metacycle_LS_BH.Q : An estimated FDR vaue from LS 
15. metacycle_LS_period : An estimated period from LS 
16. metacycle_LS_adjphase : An estimated lag from LS 
17. metacycle_LS_amplitude : An estimated amplitude from LS 
18. metacycle_meta2d_pvalue : An estimated p value from metacycle
19. metacycle_meta2d_BH.Q : An estimated FDR from metacycle
20. metacycle_meta2d_period : An estimated period from metacycle
21. metacycle_meta2d_phase : An estimated lag from metacycle
22. metacycle_meta2d_Base : An estimated mimium expression from metacycle
23. metacycle_meta2d_AMP : an estimated amplitude from metacycle
24. metacycle_meta2d_rAMP : an estimated relative amplitude (AMP/Base) from metacycle
