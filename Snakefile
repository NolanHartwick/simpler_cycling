import pandas
from snakemake.utils import validate


validate(config, "schema/config.schema.json")

counts = config["counts"]

time_points = config.get("time_points")
if(time_points is None):
    time_points = open(counts).readline().rstrip().split("\t")[1:]
    # df_counts = pandas.read_csv(counts, sep="\t", index_col=0)
    # time_points = df_counts.columns
orig_time_points = time_points
time_points = [int(h) % 24 for h in time_points]


periods = config["model_params"]["periods"]
phase_step = config["model_params"]["phase_step"]
spike_step = config["model_params"]["spike_step"]


rule all:
    input:
        "cycling_results.tsv"


rule normalize_counts:
    input:
        counts
    output:
        "counts.normed.tsv"
    params:
        norm = config["norm"],
        threshold = config["filter"]
    conda:
        "envs/general.yml"
    script:
        "scripts/norm_counts.py"


rule make_resamples:
    input:
        rules.normalize_counts.output[0]
    output:
        resamps = [temp(f"counts.resample_{n}.tsv") for n in range(config["resamples"])],
    conda:
        "envs/general.yml"
    script:
        "scripts/prep_counts.py"


rule gen_models:
    input:
        []
    output:
        "haystack_models.tsv"
    params:
        timepoints = time_points,
        sinspace = [(ph, per) for per in periods for ph in range(0, per, phase_step)],
        spikespace = [
            (ph, per, spike_step, hw)
            for per in periods
            for ph in range(0, per, phase_step)
            for hw in range(int(spike_step / 2), per - spike_step * 2, spike_step)
            if(hw + 2 * spike_step <= per)
        ] + [
            (ph, per, per / 2, 0)
            for per in periods
            for ph in range(0, per, phase_step)
        ]
    conda:
        "envs/general.yml"
    script:
        "scripts/gen_models.py"


rule haystack:
    input:
        counts = 'counts.{base}.tsv',
        models = rules.gen_models.output[0]
    output:
        temp("haystack_results.{base}.tsv")
    params:
        haystack_path = os.path.join(workflow.basedir, "external", "haystack.pl")
    conda:
        "envs/general.yml"
    shell:
        """
        perl {params.haystack_path} -m {input.models} -d {input.counts} -c -2 -f -2 -r 2 -b -1 -n {output[0]}
        """


rule clean_haystack:
    input:
        users = rules.haystack.output[0].format(base="normed"),
        resamps = [rules.haystack.output[0].format(base=f"resample_{n}") for n in range(config["resamples"])]
    output:
        "haystack.summary.tsv",
        "haystack.full.tsv"
    params:
        time_points = time_points
    conda:
        "envs/general.yml"
    script:
        "scripts/clean_haystack.py"


rule prep_metacycle:
    input:
        rules.normalize_counts.output[0]
    output:
        temp("counts.meta.tsv")
    params:
        time_points = "\t" + "\t".join(str(i) for i in time_points)
    shell:
        '''
        echo '{params.time_points}' > {output}
        cat {input} >> {output}
        '''

rule metacycle:
    input:
        rules.prep_metacycle.output[0]
    output:
        "meta2d_" + rules.prep_metacycle.output[0]
    params:
        minper = min(periods),
        maxper = max(periods),
        timepoints = time_points
    conda:
        "envs/general.yml"
    script:
        "scripts/do_metacycle.R"


rule clean_metacycle:
    input:
        rules.metacycle.output[0]
    output:
        "meta2d.summary.tsv"
    conda:
        "envs/general.yml"
    script:
        "scripts/clean_metacycle.py"


rule merge_output:
    input:
        rules.metacycle.output[0],
        rules.clean_haystack.output[0],
        rules.normalize_counts.output[0]
    output:
        rules.all.input[0]
    params:
        time_points = time_points
    conda:
        "envs/general.yml"
    script:
        "scripts/merge_output.py"
